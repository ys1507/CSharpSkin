﻿using CSharpSkin.ComboBox.Struct;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CSharpSkin.ComboBox
{
    #region 操作系统API
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 操作系统API
    /// </summary>
    public class CSharpWinapi
    {
        #region 常量
       
        public const int WM_PAINT = 0x000F;
        #endregion

        #region  获取下拉框信息
        /// <summary>
        /// 获取下拉框信息
        /// </summary>
        /// <param name="hwndCombo"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetComboBoxInfo(
            IntPtr hwndCombo, ref COMBOBOXINFO info);
        #endregion

        #region 获取窗口尺寸信息
        /// <summary>
        /// 获取窗口尺寸信息
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="lpRect"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowRect(IntPtr hwnd, ref RECT lpRect);
        #endregion

        #region 开始绘制
        /// <summary>
        /// 开始绘制
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="ps"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);
        #endregion

        #region 结束绘制
        /// <summary>
        /// 结束绘制
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="ps"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);
        #endregion
    }
    #endregion
}
