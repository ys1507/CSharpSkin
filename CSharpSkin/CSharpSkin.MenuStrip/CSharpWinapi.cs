﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CSharpSkin.MenuStrip
{
    #region 操作系统API
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 操作系统API
    /// </summary>
    public class CSharpWinapi
    {
        #region 设置控件指定坐标颜色
        /// <summary>
        /// 设置控件指定坐标颜色
        /// </summary>
        /// <param name="hdc"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="crColor"></param>
        /// <returns></returns>
        [DllImport("gdi32.dll")]
        public static extern uint SetPixel(IntPtr hdc, int X, int Y, int crColor);
        #endregion
    }
    #endregion
}
