﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CSharpSkin.MenuStrip
{
    #region Strip颜色风格
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// Strip颜色风格
    /// </summary>
    [Serializable]
    public class StripColorStyle
    {
        #region 颜色风格字段
        /// <summary>
        /// 颜色风格字段
        /// </summary>
        private Color _baseColor = Color.FromArgb(105, 200, 254);
        private  Color _borderColor = Color.FromArgb(194, 169, 120);
        private  Color _backNormalColor = Color.FromArgb(250, 250, 250);
        private  Color _backHoverColor = Color.FromArgb(255, 201, 15);
        private  Color _backPressedColor = Color.FromArgb(226, 176, 0);
        private  Color _foreColor = Color.FromArgb(21, 66, 139);
        private  Color _dropDownImageBackColor = Color.FromArgb(233, 238, 238);
        private  Color _dropDownImageSeparatorColor = Color.FromArgb(197, 197, 197);
        #endregion

        #region 基础颜色
        /// <summary>
        /// 基础颜色
        /// </summary>
        public Color BaseColor
        {
            get => _baseColor;
            set
            {
                _baseColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 边框颜色
        /// <summary>
        /// 边框颜色
        /// </summary>
        public Color BorderColor
        {
            get => _borderColor;
            set
            {
                _borderColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 背景颜色
        /// <summary>
        /// 背景颜色
        /// </summary>
        public Color BackNormalColor
        {
            get => _backNormalColor;
            set
            {
                _backNormalColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 背景鼠标进入颜色
        /// <summary>
        /// 背景鼠标进入颜色
        /// </summary>
        public Color BackHoverColor
        {
            get => _backHoverColor;
            set
            {
                _backHoverColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 背景鼠标按下颜色
        /// <summary>
        /// 背景鼠标按下颜色
        /// </summary>
        public Color BackPressedColor
        {
            get => _backPressedColor;
            set
            {
                _backPressedColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 字体颜色
        /// <summary>
        /// 字体颜色
        /// </summary>
        public Color ForeColor
        {
            get => _foreColor;
            set
            {
                _foreColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 下拉图片背景颜色
        /// <summary>
        /// 下拉图片背景颜色
        /// </summary>
        public Color DropDownImageBackColor
        {
            get => _dropDownImageBackColor;
            set {
                _dropDownImageBackColor = value; ColorChangedEvent();
            }
        }
        #endregion

        #region 下拉分割条颜色
        /// <summary>
        /// 下拉分割条颜色
        /// </summary>
        public Color DropDownImageSeparatorColor
        {
            get => _dropDownImageSeparatorColor;
            set
            {
                _dropDownImageSeparatorColor = value;
                ColorChangedEvent();
            }
        }
        #endregion

        #region 定义颜色改变委托与事件
        /// <summary>
        /// 定义颜色改变委托与事件
        /// </summary>

        public delegate void ColorChangedHanlder();
        /// <summary>
        /// 颜色改变时触发
        /// </summary>
        public event ColorChangedHanlder OnColorChangedEventHanlder;

        private void ColorChangedEvent()
        {
            if (OnColorChangedEventHanlder!=null)
            {
                OnColorChangedEventHanlder.Invoke();
            }
        }
        #endregion
    }
    #endregion
}
