﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;

namespace CSharpSkin.MenuStrip.ProperityGrid
{
    #region 工具栏，状态栏，菜单栏颜色样式设计编辑属性，弹出属性编辑框
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 工具栏，状态栏，菜单栏颜色样式设计编辑属性，弹出属性编辑框
    /// </summary>
    public class StripColorStylePropertyEditor:UITypeEditor
    {
        #region 指定为模式窗体属性编辑器类型
        /// <summary>
        /// 指定为模式窗体属性编辑器类型
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
        #endregion

        #region 编辑属性值
        /// <summary>
        /// 编辑属性值
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (provider != null)
            {
                IWindowsFormsEditorService svc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                if (svc != null && context.Instance != null)
                {
                    //取出设计器所在的窗体(组件/控件所在窗体) 
                    IDesignerHost host = (IDesignerHost)provider.GetService(typeof(IDesignerHost));
                    if (host == null) return value;
                    //context.Instance:可以得到当前的Component1组件。 
                    CSharpStripRenderComponent stripRenderComponent = context.Instance as CSharpStripRenderComponent;
                    StripColorStyle stripColorStyle = stripRenderComponent.StripColorStyle;
                    if (stripColorStyle == null) stripColorStyle = new StripColorStyle();
                    StripColorStyleEditor stripColorStyleEditor = new StripColorStyleEditor(stripColorStyle);
                    svc.ShowDialog(stripColorStyleEditor);//打开属性编辑窗体 
                    stripColorStyleEditor.Dispose();//释放内存 
                    //重新序列化内容到.Designer.cs文件 
                    stripRenderComponent.StripColorStyle = stripColorStyleEditor.StripColorStyle;
                    context.PropertyDescriptor.SetValue(context.Instance, stripRenderComponent.StripColorStyle);
                    return stripRenderComponent.StripColorStyle;//返回修改后的对象 
                }
            }
            return value;
        }
        #endregion
    }
    #endregion
}
