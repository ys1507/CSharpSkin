﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing;
using CSharpSkin.MenuStrip.Gdi;
using CSharpSkin.MenuStrip.Class;
using ControlPaint = CSharpSkin.MenuStrip.Class.ControlPaint;
using System.Windows.Forms.ComponentModel;
using System.ComponentModel;

namespace CSharpSkin.MenuStrip.Render
{
    #region Strip渲染风格
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// Strip渲染风格
    /// </summary>
    public class CSharpStripRenderer : ToolStripRenderer
    {
        private static readonly int OffsetMargin = 24;

        #region 颜色风格
        private StripColorStyle _stripColorStyle = new StripColorStyle();
        /// <summary>
        ///颜色风格
        /// </summary>
        public StripColorStyle StripColorStyle
        {
            get
            {
                if (_stripColorStyle == null)
                {
                    _stripColorStyle = new StripColorStyle();
                }
                return _stripColorStyle;
            }
            set
            {
                _stripColorStyle = value;
            }
        }
        #endregion
 
        #region  构造 无参
        /// <summary>
        /// 构造 无参
        /// </summary>
        public CSharpStripRenderer()
            : base()
        {
           
        }
        #endregion

        #region  构造 有参
        
        /// <summary>
        /// 构造 有参
        /// </summary>
        public CSharpStripRenderer(StripColorStyle stripColorStyle) : base()
        {
            _stripColorStyle = stripColorStyle;
            ToolStripManager.Renderer = this;
        }
        #endregion

        #region 重写绘制背景
        /// <summary>
        /// 重写绘制背景
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Graphics g = e.Graphics;
            Rectangle bounds = e.AffectedBounds;

            if (toolStrip is ToolStripDropDown)
            {
                RegionManager.CreateRegion(toolStrip, bounds);
                using (SolidBrush brush = new SolidBrush(this._stripColorStyle.BackNormalColor))
                {
                    g.FillRectangle(brush, bounds);
                }
            }
            else
            {
                LinearGradientMode mode = toolStrip.Orientation == Orientation.Horizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
                ControlRender.RenderBackground(g,bounds,this._stripColorStyle.BaseColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.None,0,0.35f,false,false,mode);
            }
            DrawControlBorder(g,bounds);
        }
        #endregion

        #region 绘制Panel边框
        /// <summary>
        /// 绘制Panel边框
        /// </summary>
        /// <param name="panel"></param>
        private void DrawControlBorder(Graphics graphics, Rectangle rectangle)
        {
            Pen pen = new Pen(this._stripColorStyle.BorderColor, 1);
            rectangle.X += 0;
            rectangle.Y += 0;
            rectangle.Width -= 1 * 2;
            rectangle.Height -= 1 * 2;
            graphics.DrawRectangle(pen, rectangle);

        }
        #endregion

        #region 重写图片边距
        /// <summary>
        /// 重写图片边距
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderImageMargin(ToolStripRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Graphics g = e.Graphics;
            Rectangle bounds = e.AffectedBounds;
            if (toolStrip is ToolStripDropDown)
            {
                bool bRightToLeft = toolStrip.RightToLeft == RightToLeft.Yes;
                Rectangle imageBackRect = bounds;
                imageBackRect.Width = OffsetMargin;
                if (bRightToLeft)
                {
                    imageBackRect.X -= 3;
                }
                else
                {
                    imageBackRect.X += 3;
                }

                imageBackRect.Y += 2;
                imageBackRect.Height -= 4;
                using (SolidBrush brush = new SolidBrush(this._stripColorStyle.DropDownImageBackColor))
                {
                    g.FillRectangle(brush, imageBackRect);
                }
                Point ponitStart;
                Point pointEnd;
                if (bRightToLeft)
                {
                    ponitStart = new Point(imageBackRect.X, imageBackRect.Y);
                    pointEnd = new Point(imageBackRect.X, imageBackRect.Bottom);
                }
                else
                {
                    ponitStart = new Point(imageBackRect.Right - 1, imageBackRect.Y);
                    pointEnd = new Point(imageBackRect.Right - 1, imageBackRect.Bottom);
                }
                using (Pen pen = new Pen(this._stripColorStyle.DropDownImageSeparatorColor))
                {
                    g.DrawLine(pen, ponitStart, pointEnd);
                }
            }
            else
            {
                base.OnRenderImageMargin(e);
            }
        }
        #endregion

        #region 重写边框
        /// <summary>
        /// 重写边框
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Graphics g = e.Graphics;
            Rectangle bounds = e.AffectedBounds;
            if (toolStrip is ToolStripDropDown)
            {
                g.SmoothingMode = SmoothingMode.HighQuality;
                using (GraphicsPath path = GraphicsPathManager.CreatePath(bounds, 8, RoundStyle.All, true))
                {
                    using (Pen pen = new Pen(this._stripColorStyle.DropDownImageSeparatorColor))
                    {
                        path.Widen(pen);
                        g.DrawPath(pen, path);
                    }
                }
                if (!(toolStrip is ToolStripOverflow))
                {
                    bounds.Inflate(-1, -1);
                    using (GraphicsPath innerPath = GraphicsPathManager.CreatePath(bounds, 8, RoundStyle.All, true))
                    {
                        using (Pen pen = new Pen(this._stripColorStyle.BackNormalColor))
                        {
                            g.DrawPath(pen, innerPath);
                        }
                    }
                }
            }
            else
            {
                base.OnRenderToolStripBorder(e);
            }
        }
        #endregion

        #region 绘制项背景
        /// <summary>
        /// 绘制项背景
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            ToolStripItem item = e.Item;

            if (!item.Enabled)
            {
                return;
            }
            Graphics g = e.Graphics;
            Rectangle rect = new Rectangle(Point.Empty, e.Item.Size);
            if (toolStrip is System.Windows.Forms.MenuStrip)
            {
                LinearGradientMode mode =toolStrip.Orientation == Orientation.Horizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
                if (item.Selected)
                {
                    ControlRender.RenderBackground(g,rect,this._stripColorStyle.BackHoverColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                }
                else if (item.Pressed)
                {
                    ControlRender.RenderBackground(g,rect,this._stripColorStyle.BackPressedColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                }
                else
                {
                    base.OnRenderMenuItemBackground(e);
                }
            }
            else if (toolStrip is ToolStripDropDown)
            {
                int offsetMargin = 0;
                if (item.RightToLeft == RightToLeft.Yes)
                {
                    rect.X += 4;
                }
                else
                {
                    rect.X += offsetMargin + 4;
                }
                rect.Width -= offsetMargin + 8;
                rect.Height--;
                if (item.Selected)
                {
                    ControlRender.RenderBackground(g,rect,this._stripColorStyle.BackHoverColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,LinearGradientMode.Vertical);
                }
                else
                {
                    base.OnRenderMenuItemBackground(e);
                }
            }
            else
            {
                base.OnRenderMenuItemBackground(e);
            }
        }
        #endregion

        #region 绘制项图片
        /// <summary>
        /// 绘制项图片
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderItemImage(ToolStripItemImageRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Graphics g = e.Graphics;
            if (toolStrip is ToolStripDropDown &&e.Item is ToolStripMenuItem)
            {
                int offsetMargin = 0;
                ToolStripMenuItem item = (ToolStripMenuItem)e.Item;
                if (item.Checked)
                {
                    return;
                }
                Rectangle rect = e.ImageRectangle;
                if (e.Item.RightToLeft == RightToLeft.Yes)
                {
                    rect.X -= offsetMargin + 2;
                }
                else
                {
                    rect.X += offsetMargin + 2;
                }
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                ToolStripItemImageRenderEventArgs ne =new ToolStripItemImageRenderEventArgs(g, e.Item, e.Image, rect);
                base.OnRenderItemImage(ne);
              
            }
            else
            {
                base.OnRenderItemImage(e);
            }
        }
        #endregion

        #region 绘制项文本
        /// <summary>
        /// 绘制项文本
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            e.TextColor = this._stripColorStyle.ForeColor;
            if (toolStrip is ToolStripDropDown && e.Item is ToolStripMenuItem)
            {
                int offsetMargin =  0;
                Rectangle rect = e.TextRectangle;
                if (e.Item.RightToLeft == RightToLeft.Yes)
                {
                    rect.X -= offsetMargin;
                }
                else
                {
                    rect.X += offsetMargin;
                }
                e.TextRectangle = rect;
            }
            base.OnRenderItemText(e);
        }
        #endregion

        #region 绘制项选中
        /// <summary>
        /// 绘制项选中
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderItemCheck(ToolStripItemImageRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Graphics g = e.Graphics;
            if (toolStrip is ToolStripDropDown && e.Item is ToolStripMenuItem)
            {
                int offsetMargin = 0;
                Rectangle rect = e.ImageRectangle;
                if (e.Item.RightToLeft == RightToLeft.Yes)
                {
                    rect.X -= offsetMargin + 2;
                }
                else
                {
                    rect.X += offsetMargin + 2;
                }
                rect.Width = 13;
                rect.Y += 1;
                rect.Height -= 3;
                g.SmoothingMode = SmoothingMode.HighQuality;

                using (GraphicsPath path = new GraphicsPath())
                {
                    path.AddRectangle(rect);
                    using (PathGradientBrush brush = new PathGradientBrush(path))
                    {
                        brush.CenterColor = this._stripColorStyle.DropDownImageBackColor;
                        brush.SurroundColors = new Color[] { this._stripColorStyle.BackPressedColor };
                        Blend blend = new Blend();
                        blend.Positions = new float[] { 0f, 0.3f, 1f };
                        blend.Factors = new float[] { 0f, 0.5f, 1f };
                        brush.Blend = blend;
                        g.FillRectangle(brush, rect);
                    }
                }

                using (Pen pen = new Pen(this._stripColorStyle.BackPressedColor))
                {
                    g.DrawRectangle(pen, rect);
                }
                Class.ControlPaint.DrawChecked(g, rect, this._stripColorStyle.ForeColor);

            }
            else
            {
                base.OnRenderItemCheck(e);
            }
        }
        #endregion

        #region 绘制箭头
        /// <summary>
        /// 绘制箭头
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            if (e.Item.Enabled)
            {
                e.ArrowColor = this._stripColorStyle.ForeColor;
            }

            ToolStrip toolStrip = e.Item.Owner;

            if (toolStrip is ToolStripDropDown &&e.Item is ToolStripMenuItem)
            {
                
                int offsetMargin =  0;
                Rectangle rect = e.ArrowRectangle;
                if (e.Item.RightToLeft == RightToLeft.Yes)
                {
                    rect.X -= offsetMargin;
                }
                else
                {
                    rect.X += offsetMargin;
                }

                e.ArrowRectangle = rect;
            }

            base.OnRenderArrow(e);
        }
        #endregion

        #region 绘制分割线条
        /// <summary>
        /// 绘制分割线条
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Rectangle rect = e.Item.ContentRectangle;
            Graphics g = e.Graphics;
            Color baseColor = this._stripColorStyle.BaseColor;
            if (toolStrip is ToolStripDropDown)
            {
                int offsetMargin = OffsetMargin;
                if (e.Item.RightToLeft != RightToLeft.Yes)
                {
                    rect.X += offsetMargin + 2;
                }
                rect.Width -= offsetMargin + 4;
                baseColor = this._stripColorStyle.DropDownImageSeparatorColor;
            }
            RenderSeparatorLine(g,rect,baseColor,this._stripColorStyle.BackNormalColor,Color.Snow,e.Vertical);
        }
        #endregion

        #region 绘制按钮背景
        /// <summary>
        /// 绘制按钮背景
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            ToolStripButton item = e.Item as ToolStripButton;
            Graphics g = e.Graphics;
            if (item != null)
            {
                LinearGradientMode mode =toolStrip.Orientation == Orientation.Horizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
                g.SmoothingMode = SmoothingMode.HighQuality;
                Rectangle bounds = new Rectangle(Point.Empty, item.Size);
                if (item.BackgroundImage != null)
                {
                    Rectangle clipRect = item.Selected ? item.ContentRectangle : bounds;
                    ControlPaint.DrawBackgroundImage(g,item.BackgroundImage,this._stripColorStyle.BackNormalColor,item.BackgroundImageLayout,bounds, clipRect);
                }
                if (item.CheckState == CheckState.Unchecked)
                {
                    if (item.Selected)
                    {
                        Color color = this._stripColorStyle.BackHoverColor;
                        if (item.Pressed)
                        {
                            color = this._stripColorStyle.BackPressedColor;
                        }
                        ControlRender.RenderBackground(g,bounds,color,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                    }
                    else
                    {
                        if (toolStrip is ToolStripOverflow)
                        {
                            using (Brush brush = new SolidBrush(this._stripColorStyle.BackHoverColor))
                            {
                                g.FillRectangle(brush, bounds);
                            }
                        }
                    }
                }
                else
                {
                    Color color = System.Windows.Forms.ControlPaint.Light(this._stripColorStyle.BackHoverColor);
                    if (item.Selected)
                    {
                        color = this._stripColorStyle.BackHoverColor;
                    }
                    if (item.Pressed)
                    {
                        color = this._stripColorStyle.BackPressedColor;
                    }
                    ControlRender.RenderBackground(g,bounds,color,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                }

                 
            }
        }
        #endregion

        #region 绘制下拉按钮背景
        /// <summary>
        /// 绘制下拉按钮背景
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderDropDownButtonBackground(ToolStripItemRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            ToolStripDropDownItem item = e.Item as ToolStripDropDownItem;
            if (item != null)
            {
                LinearGradientMode mode =toolStrip.Orientation == Orientation.Horizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
                Graphics g = e.Graphics;
                g.SmoothingMode = SmoothingMode.HighQuality;
                Rectangle bounds = new Rectangle(Point.Empty, item.Size);
                if (item.Pressed && item.HasDropDownItems)
                {
                    ControlRender.RenderBackground(g,bounds,this._stripColorStyle.BackPressedColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                }
                else if (item.Selected)
                {
                    ControlRender.RenderBackground(g,bounds,this._stripColorStyle.BackHoverColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                }
                else if (toolStrip is ToolStripOverflow)
                {
                    using (Brush brush = new SolidBrush(this._stripColorStyle.BackNormalColor))
                    {
                        g.FillRectangle(brush, bounds);
                    }
                }
                else
                {
                    base.OnRenderDropDownButtonBackground(e);
                }
 
            }
        }
        #endregion

        #region 绘制分割按钮背景
        /// <summary>
        /// 绘制分割按钮背景
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderSplitButtonBackground(ToolStripItemRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            ToolStripSplitButton item = e.Item as ToolStripSplitButton;
            if (item != null)
            {
                Graphics g = e.Graphics;
                LinearGradientMode mode =toolStrip.Orientation == Orientation.Horizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
                Rectangle bounds = new Rectangle(Point.Empty, item.Size);
                g.SmoothingMode = SmoothingMode.HighQuality;
                Color arrowColor = toolStrip.Enabled ? this._stripColorStyle.ForeColor : SystemColors.ControlDark;
                if (item.BackgroundImage != null)
                {
                    Rectangle clipRect = item.Selected ? item.ContentRectangle : bounds;
                    ControlPaint.DrawBackgroundImage(g,item.BackgroundImage,this._stripColorStyle.BackNormalColor,item.BackgroundImageLayout,bounds,clipRect);
                }

                if (item.ButtonPressed)
                {
                    Rectangle buttonBounds = item.ButtonBounds;
                    Padding padding = (item.RightToLeft == RightToLeft.Yes) ?
                        new Padding(0, 1, 1, 1) : new Padding(1, 1, 0, 1);
                    buttonBounds = RectangleManager.PaddingRect(buttonBounds, padding);
                    ControlRender.RenderBackground(g,bounds,this._stripColorStyle.BackHoverColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                    buttonBounds.Inflate(-1, -1);
                    g.SetClip(buttonBounds);
                    ControlRender.RenderBackground(g,buttonBounds,this._stripColorStyle.BackPressedColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.Left,false,true,mode);
                    g.ResetClip();
                    using (Pen pen = new Pen(this._stripColorStyle.BorderColor))
                    {
                        g.DrawLine(pen,item.SplitterBounds.Left,item.SplitterBounds.Top,item.SplitterBounds.Left,item.SplitterBounds.Bottom);
                    }
                    base.DrawArrow(new ToolStripArrowRenderEventArgs(g,item,item.DropDownButtonBounds,arrowColor, ArrowDirection.Down));
                    return;
                }

                if (item.Pressed || item.DropDownButtonPressed)
                {
                    ControlRender.RenderBackground(g,bounds,this._stripColorStyle.BackPressedColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                    base.DrawArrow(new ToolStripArrowRenderEventArgs(g,item,item.DropDownButtonBounds,arrowColor,ArrowDirection.Down));
                    return;
                }
                if (item.Selected)
                {
                    ControlRender.RenderBackground(g,bounds,this._stripColorStyle.BackHoverColor,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.All,true,true,mode);
                    using (Pen pen = new Pen(this._stripColorStyle.BorderColor))
                    {
                        g.DrawLine(pen,item.SplitterBounds.Left,item.SplitterBounds.Top,item.SplitterBounds.Left,item.SplitterBounds.Bottom);
                    }
                    base.DrawArrow(
                        new ToolStripArrowRenderEventArgs(g,item,item.DropDownButtonBounds,arrowColor,ArrowDirection.Down));
                    return;
                }

                base.DrawArrow(new ToolStripArrowRenderEventArgs(g,item,item.DropDownButtonBounds,arrowColor,ArrowDirection.Down));
                return;
            }

            base.OnRenderSplitButtonBackground(e);
        }
        #endregion

        #region 绘制溢出按钮背景
        /// <summary>
        /// 绘制溢出按钮背景
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderOverflowButtonBackground(ToolStripItemRenderEventArgs e)
        {
            ToolStripItem item = e.Item;
            ToolStrip toolStrip = e.ToolStrip;
            Graphics g = e.Graphics;
            bool rightToLeft = item.RightToLeft == RightToLeft.Yes;
            g.SmoothingMode = SmoothingMode.HighQuality;
            RenderOverflowBackground(e, rightToLeft);
            bool bHorizontal = toolStrip.Orientation == Orientation.Horizontal;
            Rectangle empty = Rectangle.Empty;
            if (rightToLeft)
            {
                empty = new Rectangle(0, item.Height - 8, 10, 5);
            }
            else
            {
                empty = new Rectangle(item.Width - 12, item.Height - 8, 10, 5);
            }
            ArrowDirection direction = bHorizontal ? ArrowDirection.Down : ArrowDirection.Right;
            int x = (rightToLeft && bHorizontal) ? -1 : 1;
            empty.Offset(x, 1);
            Color arrowColor = toolStrip.Enabled ?this._stripColorStyle.ForeColor : SystemColors.ControlDark;
            using (Brush brush = new SolidBrush(arrowColor))
            {
                ControlRender.RenderArrow(g, empty, direction, brush);
            }
            if (bHorizontal)
            {
                using (Pen pen = new Pen(arrowColor))
                {
                    g.DrawLine(pen,empty.Right - 8,empty.Y - 2,empty.Right - 2,empty.Y - 2);
                    g.DrawLine(pen,empty.Right - 8,empty.Y - 1,empty.Right - 2,empty.Y - 1);
                }
            }
            else
            {
                using (Pen pen = new Pen(arrowColor))
                {
                    g.DrawLine(pen,empty.X,empty.Y,empty.X,empty.Bottom - 1);
                    g.DrawLine(pen,empty.X,empty.Y + 1,empty.X,empty.Bottom);
                }
            }
        }
        #endregion

        #region 绘制控件
        /// <summary>
        /// 绘制控件
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderGrip(ToolStripGripRenderEventArgs e)
        {
            if (e.GripStyle == ToolStripGripStyle.Visible)
            {
                Rectangle bounds = e.GripBounds;
                bool isVertical = e.GripDisplayStyle == ToolStripGripDisplayStyle.Vertical;
                ToolStrip toolStrip = e.ToolStrip;
                Graphics g = e.Graphics;
                if (isVertical)
                {
                    bounds.X = e.AffectedBounds.X;
                    bounds.Width = e.AffectedBounds.Width;
                    if (toolStrip is System.Windows.Forms.MenuStrip)
                    {
                        if (e.AffectedBounds.Height > e.AffectedBounds.Width)
                        {
                            isVertical = false;
                            bounds.Y = e.AffectedBounds.Y;
                        }
                        else
                        {
                            toolStrip.GripMargin = new Padding(0, 2, 0, 2);
                            bounds.Y = e.AffectedBounds.Y;
                            bounds.Height = e.AffectedBounds.Height;
                        }
                    }
                    else
                    {
                        toolStrip.GripMargin = new Padding(2, 2, 4, 2);
                        bounds.X++;
                        bounds.Width++;
                    }
                }
                else
                {
                    bounds.Y = e.AffectedBounds.Y;
                    bounds.Height = e.AffectedBounds.Height;
                }

                DrawDottedGrip(g,bounds, isVertical, false,this._stripColorStyle.BackNormalColor,System.Windows.Forms.ControlPaint.Dark(this._stripColorStyle.BaseColor, 0.3F));
            }
        }
        #endregion

        #region 绘制状态栏
        /// <summary>
        /// 绘制状态栏
        /// </summary>
        /// <param name="e"></param>
        protected override void OnRenderStatusStripSizingGrip(ToolStripRenderEventArgs e)
        {
            DrawSolidStatusGrip(e.Graphics,e.AffectedBounds,this._stripColorStyle.BackNormalColor,System.Windows.Forms.ControlPaint.Dark(this._stripColorStyle.BaseColor, 0.3f));
        }
        #endregion

        #region 绘制分割线
        /// <summary>
        /// 绘制分割线
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="baseColor"></param>
        /// <param name="backColor"></param>
        /// <param name="shadowColor"></param>
        /// <param name="vertical"></param>
        void RenderSeparatorLine(Graphics g,Rectangle rect,Color baseColor,Color backColor,Color shadowColor,bool vertical)
        {
            if (vertical)
            {
                rect.Y += 2;
                rect.Height -= 4;
                using (LinearGradientBrush brush = new LinearGradientBrush(rect,baseColor,backColor,LinearGradientMode.Vertical))
                {
                    using (Pen pen = new Pen(brush))
                    {
                        g.DrawLine(pen, rect.X, rect.Y, rect.X, rect.Bottom);
                    }
                }
            }
            else
            {
                using (LinearGradientBrush brush = new LinearGradientBrush(rect,baseColor,backColor,180F))
                {
                    Blend blend = new Blend();
                    blend.Positions = new float[] { 0f, .2f, .5f, .8f, 1f };
                    blend.Factors = new float[] { 1f, .3f, 0f, .3f, 1f };
                    brush.Blend = blend;
                    using (Pen pen = new Pen(brush))
                    {
                        g.DrawLine(pen, rect.X, rect.Y, rect.Right, rect.Y);
                        brush.LinearColors = new Color[] {
                        shadowColor, backColor };
                        pen.Brush = brush;
                        g.DrawLine(pen, rect.X, rect.Y + 1, rect.Right, rect.Y + 1);
                    }
                }
            }
        }
        #endregion

        #region 绘制溢出背景
        /// <summary>
        /// 绘制溢出背景
        /// </summary>
        /// <param name="e"></param>
        /// <param name="rightToLeft"></param>
        void RenderOverflowBackground(ToolStripItemRenderEventArgs e,bool rightToLeft)
        {
            Color color = Color.Empty;
            Graphics g = e.Graphics;
            ToolStrip toolStrip = e.ToolStrip;
            ToolStripOverflowButton item = e.Item as ToolStripOverflowButton;
            Rectangle bounds = new Rectangle(Point.Empty, item.Size);
            Rectangle withinBounds = bounds;
            bool bParentIsMenuStrip = !(item.GetCurrentParent() is System.Windows.Forms.MenuStrip);
            bool bHorizontal = toolStrip.Orientation == Orientation.Horizontal;
            if (bHorizontal)
            {
                bounds.X += (bounds.Width - 12) + 1;
                bounds.Width = 12;
                if (rightToLeft)
                {
                    bounds = RectangleManager.TranslatePositionX(bounds, withinBounds);
                }
            }
            else
            {
                bounds.Y = (bounds.Height - 12) + 1;
                bounds.Height = 12;
            }
            if (item.Pressed)
            {
                color = this._stripColorStyle.BackPressedColor;
            }
            else if (item.Selected)
            {
                color = this._stripColorStyle.BackHoverColor;
            }
            else
            {
                color = this._stripColorStyle.BaseColor;
            }
            if (bParentIsMenuStrip)
            {
                using (Pen pen = new Pen(this._stripColorStyle.BaseColor))
                {
                    Point point = new Point(bounds.Left - 1, bounds.Height - 2);
                    Point point2 = new Point(bounds.Left, bounds.Height - 2);
                    if (rightToLeft)
                    {
                        point.X = bounds.Right + 1;
                        point2.X = bounds.Right;
                    }
                    g.DrawLine(pen, point, point2);
                }
            }

            LinearGradientMode mode = bHorizontal ?LinearGradientMode.Vertical : LinearGradientMode.Horizontal;
            ControlRender.RenderBackground(g,bounds,color,this._stripColorStyle.BorderColor,this._stripColorStyle.BackNormalColor,RoundStyle.None,0,0.35f,false,false,mode);
            if (bParentIsMenuStrip)
            {
                using (Brush brush = new SolidBrush(this._stripColorStyle.BaseColor))
                {
                    if (bHorizontal)
                    {
                        Point point3 = new Point(bounds.X - 2, 0);
                        Point point4 = new Point(bounds.X - 1, 1);
                        if (rightToLeft)
                        {
                            point3.X = bounds.Right + 1;
                            point4.X = bounds.Right;
                        }
                        g.FillRectangle(brush, point3.X, point3.Y, 1, 1);
                        g.FillRectangle(brush, point4.X, point4.Y, 1, 1);
                    }
                    else
                    {
                        g.FillRectangle(brush, bounds.Width - 3, bounds.Top - 1, 1, 1);
                        g.FillRectangle(brush, bounds.Width - 2, bounds.Top - 2, 1, 1);
                    }
                }
                using (Brush brush = new SolidBrush(this._stripColorStyle.BaseColor))
                {
                    if (bHorizontal)
                    {
                        Rectangle rect = new Rectangle(bounds.X - 1, 0, 1, 1);
                        if (rightToLeft)
                        {
                            rect.X = bounds.Right;
                        }
                        g.FillRectangle(brush, rect);
                    }
                    else
                    {
                        g.FillRectangle(brush, bounds.X, bounds.Top - 1, 1, 1);
                    }
                }
            }
        }
        #endregion

        #region 绘制虚线菜单栏
        /// <summary>
        /// 绘制虚线菜单栏
        /// </summary>
        /// <param name="g"></param>
        /// <param name="bounds"></param>
        /// <param name="vertical"></param>
        /// <param name="largeDot"></param>
        /// <param name="innerColor"></param>
        /// <param name="outerColor"></param>
        void DrawDottedGrip(Graphics g, Rectangle bounds, bool vertical, bool largeDot, Color innerColor, Color outerColor)
        {
            bounds.Height -= 3;
            Point position = new Point(bounds.X, bounds.Y);
            int sep;
            Rectangle posRect = new Rectangle(0, 0, 2, 2);
            g.SmoothingMode = SmoothingMode.HighQuality;
            IntPtr hdc;
            if (vertical)
            {
                sep = bounds.Height;
                position.Y += 8;
                for (int i = 0; position.Y > 4; i += 4)
                {
                    position.Y = sep - (2 + i);
                    if (largeDot)
                    {
                        posRect.Location = position;
                        DrawCircle(g, posRect, outerColor, innerColor);
                    }
                    else
                    {
                        int innerWin32Corlor = ColorTranslator.ToWin32(innerColor);
                        int outerWin32Corlor = ColorTranslator.ToWin32(outerColor);
                        hdc = g.GetHdc();
                        CSharpWinapi.SetPixel(hdc, position.X, position.Y, innerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 1, position.Y, outerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X, position.Y + 1, outerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 3, position.Y, innerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 4, position.Y, outerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 3, position.Y + 1, outerWin32Corlor);
                        g.ReleaseHdc(hdc);
                    }
                }
            }
            else
            {
                bounds.Inflate(-2, 0);
                sep = bounds.Width;
                position.X += 2;
                for (int i = 1; position.X > 0; i += 4)
                {
                    position.X = sep - (2 + i);
                    if (largeDot)
                    {
                        posRect.Location = position;
                        DrawCircle(g, posRect, outerColor, innerColor);
                    }
                    else
                    {
                        int innerWin32Corlor = ColorTranslator.ToWin32(innerColor);
                        int outerWin32Corlor = ColorTranslator.ToWin32(outerColor);
                        hdc = g.GetHdc();
                        CSharpWinapi.SetPixel(hdc, position.X, position.Y, innerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 1, position.Y, outerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X, position.Y + 1, outerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 3, position.Y, innerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 4, position.Y, outerWin32Corlor);
                        CSharpWinapi.SetPixel(hdc, position.X + 3, position.Y + 1, outerWin32Corlor);
                        g.ReleaseHdc(hdc);
                    }
                }

            }
        }
        #endregion

        #region 绘制圆形
        /// <summary>
        /// 绘制圆形
        /// </summary>
        /// <param name="g"></param>
        /// <param name="bounds"></param>
        /// <param name="borderColor"></param>
        /// <param name="fillColor"></param>
        void DrawCircle(Graphics g,Rectangle bounds,Color borderColor,Color fillColor)
        {
            using (GraphicsPath circlePath = new GraphicsPath())
            {
                circlePath.AddEllipse(bounds);
                circlePath.CloseFigure();
                using (Pen borderPen = new Pen(borderColor))
                {
                    g.DrawPath(borderPen, circlePath);
                }
                using (Brush backBrush = new SolidBrush(fillColor))
                {
                    g.FillPath(backBrush, circlePath);
                }
            }
        }
        #endregion

        #region 绘制虚线状态栏
        /// <summary>
        /// 绘制虚线状态栏
        /// </summary>
        /// <param name="g"></param>
        /// <param name="bounds"></param>
        /// <param name="innerColor"></param>
        /// <param name="outerColor"></param>
        void DrawDottedStatusGrip(Graphics g, Rectangle bounds, Color innerColor, Color outerColor)
        {
            Rectangle shape = new Rectangle(0, 0, 2, 2);
            shape.X = bounds.Width - 17;
            shape.Y = bounds.Height - 8;
            g.SmoothingMode = SmoothingMode.HighQuality;
            DrawCircle(g, shape, outerColor, innerColor);
            shape.X = bounds.Width - 12;
            DrawCircle(g, shape, outerColor, innerColor);
            shape.X = bounds.Width - 7;
            DrawCircle(g, shape, outerColor, innerColor);
            shape.Y = bounds.Height - 13;
            DrawCircle(g, shape, outerColor, innerColor);
            shape.Y = bounds.Height - 18;
            DrawCircle(g, shape, outerColor, innerColor);
            shape.Y = bounds.Height - 13;
            shape.X = bounds.Width - 12;
            DrawCircle(g, shape, outerColor, innerColor);

        }
        #endregion

        #region 绘制实现状态栏
        /// <summary>
        /// 绘制实现状态栏
        /// </summary>
        /// <param name="g"></param>
        /// <param name="bounds"></param>
        /// <param name="innerColor"></param>
        /// <param name="outerColor"></param>
        void DrawSolidStatusGrip(Graphics g, Rectangle bounds, Color innerColor, Color outerColor)
        {
            g.SmoothingMode = SmoothingMode.HighQuality;
            using (Pen innerPen = new Pen(innerColor), outerPen = new Pen(outerColor))
            {
                //outer line
                g.DrawLine(outerPen,new Point(bounds.Width - 14, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 16));
                g.DrawLine(innerPen,new Point(bounds.Width - 13, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 15));
                g.DrawLine(outerPen,new Point(bounds.Width - 12, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 14));
                g.DrawLine(innerPen,new Point(bounds.Width - 11, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 13));
                g.DrawLine(outerPen,new Point(bounds.Width - 10, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 12));
                g.DrawLine(innerPen,new Point(bounds.Width - 9, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 11));
                g.DrawLine(outerPen,new Point(bounds.Width - 8, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 10));
                g.DrawLine(innerPen,new Point(bounds.Width - 7, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 9));
                g.DrawLine(outerPen,new Point(bounds.Width - 6, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 8));
                g.DrawLine(innerPen,new Point(bounds.Width - 5, bounds.Height - 6),new Point(bounds.Width - 4, bounds.Height - 7));
            }

        }
        #endregion
    }
    #endregion
}
