﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.TabControl.Gdi
{
    #region 创建绘制路径
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 创建绘制路径
    /// </summary>
    public class GraphicsPathManager
    {
        #region 根据控件对齐方式，圆角大小绘制路径
        /// <summary>
        /// 根据控件对齐方式，圆角大小绘制路径
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="radius"></param>
        /// <param name="tabAlignment"></param>
        /// <returns></returns>
        public static GraphicsPath CreateTabPath(Rectangle rect,int radius, TabAlignment tabAlignment)
        {
            GraphicsPath path = new GraphicsPath();
            switch (tabAlignment)
            {
                case TabAlignment.Top:
                    rect.X++;
                    rect.Width -= 2;
                    path.AddLine(rect.X,rect.Bottom,rect.X,rect.Y + radius / 2);
                    path.AddArc(rect.X,rect.Y,radius,radius,180F,90F);
                    path.AddArc(rect.Right - radius,rect.Y,radius,radius,270F,90F);
                    path.AddLine(rect.Right,rect.Y + radius / 2,rect.Right,rect.Bottom);
                    break;
                case TabAlignment.Bottom:
                    rect.X++;
                    rect.Width -= 2;
                    path.AddLine(rect.X,rect.Y,rect.X,rect.Bottom - radius / 2);
                    path.AddArc(rect.X,rect.Bottom - radius,radius,radius,180,-90);
                    path.AddArc(rect.Right - radius,rect.Bottom - radius,radius,radius,90,-90);
                    path.AddLine(rect.Right,rect.Bottom - radius / 2,rect.Right,rect.Y);
                    break;
                case TabAlignment.Left:
                    rect.Y++;
                    rect.Height -= 2;
                    path.AddLine(rect.Right,rect.Y,rect.X + radius / 2,rect.Y);
                    path.AddArc(rect.X, rect.Y,radius, radius,270F,-90F);
                    path.AddArc(rect.X,rect.Bottom - radius,radius,radius,180F,-90F);
                    path.AddLine(rect.X + radius / 2,rect.Bottom,rect.Right,rect.Bottom);
                    break;
                case TabAlignment.Right:
                    rect.Y++;
                    rect.Height -= 2;
                    path.AddLine(rect.X,rect.Y,rect.Right - radius / 2,rect.Y);
                    path.AddArc(rect.Right - radius,rect.Y,radius,radius,270F,90F);
                    path.AddArc(rect.Right - radius,rect.Bottom - radius,radius,radius,0F,90F);
                    path.AddLine(rect.Right - radius / 2,rect.Bottom, rect.X,rect.Bottom);
                    break;
            }
            path.CloseFigure();
            return path;
        }
        #endregion
    }
    #endregion
}
