﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.TabControl.Event
{
    #region 选项卡切换按钮绘制事件参数
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 选项卡切换按钮绘制事件参数
    /// </summary>
    public class TabControlUpDownButtonPaintEventArgs:PaintEventArgs
    {
        #region 字段成员
        /// <summary>
        /// 字段成员
        /// </summary>
        private bool _isMouseOver;
        private bool _isMousePress;
        private bool _isMouseInUpButton;
        #endregion

        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="clipRect"></param>
        /// <param name="isMouseOver"></param>
        /// <param name="isMousePress"></param>
        /// <param name="isMouseInUpButton"></param>
        public TabControlUpDownButtonPaintEventArgs(Graphics graphics,Rectangle clipRect, bool isMouseOver,bool isMousePress,bool isMouseInUpButton) : base(graphics, clipRect)
        {
            _isMouseOver = isMouseOver;
            _isMousePress = isMousePress;
            _isMouseInUpButton = isMouseInUpButton;
        }
        #endregion

        #region 是否鼠标移入
        /// <summary>
        /// 是否鼠标移入
        /// </summary>
        public bool IsMouseOver
        {
            get { return _isMouseOver; }
        }
        #endregion

        #region 是否鼠标按下
        /// <summary>
        /// 是否鼠标按下
        /// </summary>
        public bool IsMousePress
        {
            get { return _isMousePress; }
        }
        #endregion

        #region 是否鼠标进入切换按钮
        /// <summary>
        /// 是否鼠标进入切换按钮
        /// </summary>
        public bool IsMouseInUpButton
        {
            get { return _isMouseInUpButton; }
        }
        #endregion
    }

    public delegate void TabControlUpDownButtonPaintEventHandler(object sender,TabControlUpDownButtonPaintEventArgs e);
    #endregion
}
