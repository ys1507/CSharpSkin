﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.SplitContainer
{
    #region 分割容器箭头
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 分割容器箭头
    /// </summary>
    public enum SplitArrowDirection
    {
        None = 0,
        Left = 1,
        Right = 2,
        Up = 3,
        Down = 4,
        LeftRight = 5,
        UpDown = 6
    }
    #endregion
}
