﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.SplitContainer
{
    #region 容器Pannel状态
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 容器Pannel状态
    /// </summary>
    public enum SplitPanelState
    {
        Collapsed = 0,
        Expanded = 1,
    }
    #endregion
}
