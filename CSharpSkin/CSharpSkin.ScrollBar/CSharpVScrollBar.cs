﻿using CSharpSkin.ScrollBar.Class;
using CSharpSkin.ScrollBar.Class.ColorEditor;
using CSharpSkin.ScrollBar.Event;
using CSharpSkin.ScrollBar.ProperityGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ControlPaint = CSharpSkin.ScrollBar.Class.ControlPaint;

namespace CSharpSkin.ScrollBar
{
    public class CSharpVScrollBar: VScrollBar, IOnScrollBarPaint, IScrollBar
    {
        private ScrollBarNativeWindow  _scrollBarNativeWindow;
        private ScrollBarColorStyle _scrollBarColorStyle=new ScrollBarColorStyle();

        public CSharpVScrollBar(): base()
        {
            
        }

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor(typeof(ScrollBarColorStylePropertyEditor),typeof(UITypeEditor))]
        public ScrollBarColorStyle ScrollBarColorStyle
        {
            get
            {
                if (_scrollBarColorStyle == null)
                {
                    _scrollBarColorStyle = new ScrollBarColorStyle();
                }
                return _scrollBarColorStyle;
            }
            set
            {
                _scrollBarColorStyle = value;
                base.Invalidate();
            }
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            if (_scrollBarNativeWindow != null)
            {
                _scrollBarNativeWindow.Dispose();
            }

            if (!base.DesignMode)
            {
                _scrollBarNativeWindow = new ScrollBarNativeWindow(this);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_scrollBarNativeWindow != null)
                {
                    _scrollBarNativeWindow.Dispose();
                    _scrollBarNativeWindow = null;
                }
            }
            base.Dispose(disposing);
        }

        internal protected virtual void OnPaintScrollBarTrack(PaintScrollBarTrackEventArgs e)
        {
            Graphics g = e.Graphics;
            Rectangle rect = e.TrackRectangle;
            Color baseColor = GetGray(_scrollBarColorStyle.BaseColor);
            ControlPaint.DrawScrollBarTrack(g, rect, baseColor, Color.White, e.Orientation);
        }

        internal protected virtual void OnPaintScrollBarArrow(PaintScrollBarArrowEventArgs e)
        {
            Graphics g = e.Graphics;
            Rectangle rect = e.ArrowRectangle;
            ScrollBarState scrollBarState = e.ScrollBarState;
            ArrowDirection direction = e.ArrowDirection;
            bool bHorizontal = e.Orientation == Orientation.Horizontal;
            bool bEnabled = e.IsEnabled;
            Color backColor = _scrollBarColorStyle.BackNormalColor;
            Color baseColor = _scrollBarColorStyle.BaseColor;
            Color borderColor = _scrollBarColorStyle.BorderColor;
            Color innerBorderColor = _scrollBarColorStyle.InnerBorderColor;
            Color foreColor = _scrollBarColorStyle.ForeColor;
            bool changeColor = false;
            if (bEnabled)
            {
                switch (scrollBarState)
                {
                    case ScrollBarState.Hover:
                        baseColor = _scrollBarColorStyle.BackHoverColor;
                        break;
                    case ScrollBarState.Pressed:
                        baseColor = _scrollBarColorStyle.BackPressedColor;
                        changeColor = true;
                        break;
                    default:
                        baseColor = _scrollBarColorStyle.BaseColor;
                        break;
                }
            }
            else
            {
                backColor = GetGray(backColor);
                baseColor = GetGray(_scrollBarColorStyle.BaseColor);
                borderColor = GetGray(borderColor);
                foreColor = GetGray(foreColor);
            }
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            ControlPaint.DrawScrollBarArraw(g, rect, baseColor, backColor, borderColor, innerBorderColor, foreColor, e.Orientation, direction, changeColor);

        }

        internal protected virtual void OnPaintScrollBar(PaintScrollBarEventArgs e)
        {
            bool bEnabled = e.IsEnabled;
            if (!bEnabled)
            {
                return;
            }
            Graphics g = e.Graphics;
            Rectangle rect = e.Rect;
            ScrollBarState scrollBarState = e.ScrollBarState;
            Color backColor = _scrollBarColorStyle.BackNormalColor;
            Color baseColor = _scrollBarColorStyle.BaseColor;
            Color borderColor = _scrollBarColorStyle.BorderColor;
            Color innerBorderColor = _scrollBarColorStyle.InnerBorderColor;
            bool changeColor = false;
            switch (scrollBarState)
            {
                case ScrollBarState.Hover:
                    baseColor = _scrollBarColorStyle.BackHoverColor;
                    break;
                case ScrollBarState.Pressed:
                    baseColor = _scrollBarColorStyle.BackPressedColor;
                    changeColor = true;
                    break;
                default:
                    baseColor = _scrollBarColorStyle.BaseColor;
                    break;
            }
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            ControlPaint.DrawScrollBar(g, rect, baseColor, backColor, borderColor, innerBorderColor, e.Orientation, changeColor);

        }

        private Color GetGray(Color color)
        {
            return color;// ColorConvert.RgbToGray(new RGB(color)).Color;
        }

        #region IOnScrollBarPaint 成员
        void IOnScrollBarPaint.OnPaintScrollBarArrow(PaintScrollBarArrowEventArgs e)
        {
            OnPaintScrollBarArrow(e);
        }
        void IOnScrollBarPaint.OnPaintScrollBar(PaintScrollBarEventArgs e)
        {
            OnPaintScrollBar(e);
        }
        void IOnScrollBarPaint.OnPaintScrollBarTrack(PaintScrollBarTrackEventArgs e)
        {
            OnPaintScrollBarTrack(e);
        }
        #endregion
    }
}
