﻿using System;
using System.Runtime.InteropServices;

namespace CSharpSkin.ScrollBar.Struct
{
 
    public struct WINDOWPOS
    {
        internal IntPtr hWnd;
        internal IntPtr hWndInsertAfter;
        internal int x;
        internal int y;
        internal int cx;
        internal int cy;
        internal int flags;
    }
}
