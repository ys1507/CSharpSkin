﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CSharpSkin.ScrollBar.Struct
{
    public struct INITCOMMONCONTROLSEX
    {
        public INITCOMMONCONTROLSEX(int flags)
        {
            this.dwSize = Marshal.SizeOf(typeof(INITCOMMONCONTROLSEX));
            this.dwICC = flags;
        }

        public int dwSize;
        public int dwICC;
    }
}
