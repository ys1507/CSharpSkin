﻿using System;
using System.Runtime.InteropServices;

namespace CSharpSkin.ScrollBar.Struct
{
    public struct SCROLLBARINFO
    {
        public int cbSize;
        public RECT rcScrollBar;
        public int dxyLineButton;
        public int xyThumbTop;
        public int xyThumbBottom;
        public int reserved;
        internal int[] rgstate;
    }
}
