﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar.Constant
{
    public class PAINTOPERATIONS
    {
        public const int SRCCOPY = 0x00CC0020;
        public const int SRCPAINT = 0x00EE0086;
        public const int SRCAND = 0x008800C6;
        public const int SRCINVERT = 0x00660046;
        public const int SRCERASE = 0x00440328;
        public const int NOTSRCCOPY = 0x00330008;
        public const int NOTSRCERASE = 0x001100A6;
        public const int MERGECOPY = 0x00C000CA;
        public const int MERGEPAINT = 0x00BB0226;
        public const int PATCOPY = 0x00F00021;
        public const int PATPAINT = 0x00FB0A09;
        public const int PATINVERT = 0x005A0049;
        public const int DSTINVERT = 0x00550009;
        public const int BLACKNESS = 0x00000042;
        public const int WHITENESS = 0x00FF0062;  
    }
}
