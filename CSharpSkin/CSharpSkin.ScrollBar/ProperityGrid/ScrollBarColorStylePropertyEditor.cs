﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;

namespace CSharpSkin.ScrollBar.ProperityGrid
{
    #region  ScrollBar颜色样式设计编辑属性，弹出属性编辑框
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// ScrollBar颜色样式设计编辑属性，弹出属性编辑框
    /// </summary>
    public class ScrollBarColorStylePropertyEditor : UITypeEditor
    {
        #region 指定为模式窗体属性编辑器类型
        /// <summary>
        /// 指定为模式窗体属性编辑器类型
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
        #endregion

        #region 编辑属性值
        /// <summary>
        /// 编辑属性值
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            if (provider != null)
            {
                IWindowsFormsEditorService svc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                if (svc != null && context.Instance != null)
                {
                    //取出设计器所在的窗体(组件/控件所在窗体) 
                    IDesignerHost host = (IDesignerHost)provider.GetService(typeof(IDesignerHost));
                    if (host == null) return value;
                    IScrollBar csharpScrollBar;
                    if (context.Instance is CSharpHScrollBar)
                    {
                        csharpScrollBar = context.Instance as CSharpHScrollBar;
                        
                    }
                    else
                    {
                        csharpScrollBar = context.Instance as CSharpVScrollBar; 
                    }
                    ScrollBarColorStyle scrollBarColorStyle = csharpScrollBar.ScrollBarColorStyle;
                    if (scrollBarColorStyle == null) scrollBarColorStyle = new ScrollBarColorStyle();
                    ScrollBarColorStyleEditor toolTipColorStyleEditor = new ScrollBarColorStyleEditor(scrollBarColorStyle);
                    svc.ShowDialog(toolTipColorStyleEditor);//打开属性编辑窗体 
                    toolTipColorStyleEditor.Dispose();//释放内存                                 //重新序列化内容到.Designer.cs文件 
                    csharpScrollBar.ScrollBarColorStyle = toolTipColorStyleEditor.ScrollBarColorStyle;
                    context.PropertyDescriptor.SetValue(context.Instance, csharpScrollBar.ScrollBarColorStyle);
                    return csharpScrollBar.ScrollBarColorStyle;//返回修改后的对象 
                }
            }
            return value;
        }
        #endregion
    }
    #endregion
}
