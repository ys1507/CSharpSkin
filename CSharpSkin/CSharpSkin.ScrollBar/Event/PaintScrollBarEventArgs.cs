﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.ScrollBar.Event
{
    #region 重绘滚动条事件参数
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 重绘滚动条事件参数
    /// </summary>
    public class PaintScrollBarEventArgs : IDisposable
    {
        #region 成员
        /// <summary>
        /// 成员
        /// </summary>
        private Graphics _graphics;
        private Rectangle _rect;
        private ScrollBarState _scrollBarState;
        private Orientation _orientation;
        private bool _isEnabled;
        #endregion

        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="rect"></param>
        /// <param name="scrollBarState"></param>
        /// <param name="orientation"></param>
        public PaintScrollBarEventArgs(Graphics graphics,Rectangle rect,ScrollBarState scrollBarState,Orientation orientation) : this(graphics, rect, scrollBarState, orientation, true)
        {

        }

        public PaintScrollBarEventArgs(Graphics graphics,Rectangle rect,ScrollBarState scrollBarState,Orientation orientation,bool enabled)
        {
            _graphics = graphics;
            _rect = rect;
            _scrollBarState = scrollBarState;
            _orientation = orientation;
            _isEnabled = enabled;
        }
        #endregion

        #region 画板
        /// <summary>
        /// 画板
        /// </summary>
        public Graphics Graphics
        {
            get { return _graphics; }
            set { _graphics = value; }
        }
        #endregion

        #region 滚动条区域
        /// <summary>
        /// 滚动条区域
        /// </summary>
        public Rectangle Rect
        {
            get { return _rect; }
            set { _rect = value; }
        }
        #endregion

        #region 返回滚动条状态
        /// <summary>
        /// 返回滚动条状态
        /// </summary>
        public ScrollBarState ScrollBarState
        {
            get { return _scrollBarState; }
            set { _scrollBarState = value; }
        }
        #endregion

        #region 方向
        /// <summary>
        /// 方向
        /// </summary>
        public Orientation Orientation
        {
            get { return _orientation; }
            set { _orientation = value; }
        }
        #endregion

        #region 是否可用
        /// <summary>
        /// 是否可用
        /// </summary>
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }
        #endregion

        #region 释放
        /// <summary>
        /// 释放
        /// </summary>
        public void Dispose()
        {
            _graphics = null;
        }
        #endregion
    }
    #endregion
}
