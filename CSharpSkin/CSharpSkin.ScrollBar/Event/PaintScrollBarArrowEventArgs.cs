﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.ScrollBar.Event
{
    #region 绘制滚动条箭头事件参数
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 绘制滚动条箭头事件参数
    /// </summary>
    public class PaintScrollBarArrowEventArgs : IDisposable
    {
        #region 成员
        /// <summary>
        /// 成员
        /// </summary>
        private Graphics _graphics;
        private Rectangle _arrowRect;
        private ScrollBarState  _scrollBarState;
        private ArrowDirection _arrowDirection;
        private Orientation _orientation;
        private bool _isEnabled;
        #endregion

        #region 构造
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="arrowRect"></param>
        /// <param name="scrollBarState"></param>
        /// <param name="arrowDirection"></param>
        /// <param name="orientation"></param>
        public PaintScrollBarArrowEventArgs(Graphics graphics,Rectangle arrowRect,ScrollBarState  scrollBarState,ArrowDirection arrowDirection,Orientation orientation): this(graphics,arrowRect,scrollBarState,arrowDirection,orientation,true)
        {
        }

        public PaintScrollBarArrowEventArgs(Graphics graphics,Rectangle arrowRect,ScrollBarState  scrollBarState,ArrowDirection arrowDirection,Orientation orientation,bool enabled)
        {
            _graphics = graphics;
            _arrowRect = arrowRect;
            _scrollBarState = scrollBarState;
            _arrowDirection = arrowDirection;
            _orientation = orientation;
            _isEnabled = enabled;
        }
        #endregion

        #region 画板
        /// <summary>
        /// 画板
        /// </summary>
        public Graphics Graphics
        {
            get { return _graphics; }
            set { _graphics = value; }
        }
        #endregion

        #region 箭头区域
        /// <summary>
        /// 箭头区域
        /// </summary>
        public Rectangle ArrowRectangle
        {
            get { return _arrowRect; }
            set { _arrowRect = value; }
        }
        #endregion

        #region 返回滚动条状态
        /// <summary>
        /// 返回滚动条状态
        /// </summary>
        public ScrollBarState  ScrollBarState
        {
            get { return _scrollBarState; }
            set { _scrollBarState = value; }
        }
        #endregion

        #region 箭头方向
        /// <summary>
        /// 箭头方向
        /// </summary>
        public ArrowDirection ArrowDirection
        {
            get { return _arrowDirection; }
            set { _arrowDirection = value; }
        }
        #endregion

        #region 方向
        /// <summary>
        /// 方向
        /// </summary>
        public Orientation Orientation
        {
            get { return _orientation; }
            set { _orientation = value; }
        }
        #endregion

        #region 是否可用
        /// <summary>
        /// 是否可用
        /// </summary>
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; }
        }
        #endregion

        #region 释放
        /// <summary>
        /// 释放
        /// </summary>
        public void Dispose()
        {
            _graphics = null;
        }
        #endregion
    }
    #endregion
}
