﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.ScrollBar
{
    #region 滚动条箭头方向
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 滚动条箭头方向
    /// </summary>
    public enum ScrollBarArrowDirection
    {
        None = 0,
        Left = 1,
        Right = 2,
        Up = 3,
        Down = 4,
        LeftRight = 5,
        UpDown = 6
    }
    #endregion
}
