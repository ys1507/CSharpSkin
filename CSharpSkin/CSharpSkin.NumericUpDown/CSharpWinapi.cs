﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using CSharpSkin.NumericUpDown.Struct;

namespace CSharpSkin.NumericUpDown
{
    #region 系统函数API
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 系统函数API
    /// </summary>
    public class CSharpWinapi
    {
        #region 获取设备资源
        /// <summary>
        /// 获取设备资源
        /// </summary>
        /// <param name="ptr"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowDC(IntPtr ptr);
        #endregion

        #region 释放设备资源
        /// <summary>
        /// 释放设备资源
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="hDC"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int ReleaseDC(IntPtr hwnd, IntPtr hDC);
        #endregion

        #region 开始绘制
        /// <summary>
        /// 开始绘制
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="ps"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern IntPtr BeginPaint(IntPtr hWnd, ref PAINTSTRUCT ps);
        #endregion

        #region 结束绘制
        /// <summary>
        /// 结束绘制
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="ps"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool EndPaint(IntPtr hWnd, ref PAINTSTRUCT ps);
        #endregion

        #region 设置主题
        /// <summary>
        /// 设置主题
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="pszSubAppName"></param>
        /// <param name="pszSubIdList"></param>
        /// <returns></returns>
        [DllImport("uxtheme.dll", ExactSpelling = true, CharSet = CharSet.Unicode)]
        public static extern int SetWindowTheme(IntPtr hWnd, String pszSubAppName, String pszSubIdList);
        #endregion

        [DllImport("uxtheme.dll")]
        public extern static bool IsAppThemed();

        [DllImport("user32.dll")]
        public static extern bool PtInRect([In] ref RECT lprc, Point pt);

        #region 获取窗口区域
        /// <summary>
        /// 获取窗口区域
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT rect);
        #endregion

        #region 获取鼠标位置
        /// <summary>
        /// 获取鼠标位置
        /// </summary>
        /// <param name="lpPoint"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(ref Point lpPoint);
        #endregion

        #region 获取键盘状态
        /// <summary>
        /// 获取键盘状态
        /// </summary>
        /// <param name="nVirtKey"></param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern short GetKeyState(int nVirtKey);
        #endregion
    }
    #endregion
}
