﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpSkin.NumericUpDown.Event
{
    #region 数字选择器绘制委托
    /// <summary>
    /// WWW.CSharpSkin.COM
    /// 数字选择器绘制委托
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void NumericUpDownButtonPaintEventHandler(object sender,NumericUpDownButtonPaintEventArgs e);
    #endregion
}
