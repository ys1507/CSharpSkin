﻿namespace CSharpSkin.Demo
{
    partial class ButtonDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpButton1 = new CSharpSkin.Button.CSharpButton();
            this.cSharpButton2 = new CSharpSkin.Button.CSharpButton();
            this.cSharpSwtichBox1 = new CSharpSkin.Button.CSharpSwtichBox();
            this.cSharpImageButton1 = new CSharpSkin.Button.CSharpImageButton();
            this.cSharpPictrueButton1 = new CSharpSkin.Button.CSharpPictrueButton();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpSwtichBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpPictrueButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // cSharpButton1
            // 
            this.cSharpButton1.BaseColor = System.Drawing.Color.Red;
            this.cSharpButton1.ButtonState = CSharpSkin.Button.ButtonState.Normal;
            this.cSharpButton1.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cSharpButton1.ImageWidth = 18;
            this.cSharpButton1.Location = new System.Drawing.Point(26, 36);
            this.cSharpButton1.Name = "cSharpButton1";
            this.cSharpButton1.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cSharpButton1.Radius = 8;
            this.cSharpButton1.RoundStyle = CSharpSkin.Button.Gdi.RoundStyle.All;
            this.cSharpButton1.Size = new System.Drawing.Size(114, 36);
            this.cSharpButton1.TabIndex = 0;
            this.cSharpButton1.Text = "cSharpButton1";
            this.cSharpButton1.UseVisualStyleBackColor = true;
            // 
            // cSharpButton2
            // 
            this.cSharpButton2.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.cSharpButton2.ButtonState = CSharpSkin.Button.ButtonState.Normal;
            this.cSharpButton2.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cSharpButton2.ImageWidth = 18;
            this.cSharpButton2.Location = new System.Drawing.Point(170, 36);
            this.cSharpButton2.Name = "cSharpButton2";
            this.cSharpButton2.PressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cSharpButton2.Radius = 8;
            this.cSharpButton2.RoundStyle = CSharpSkin.Button.Gdi.RoundStyle.All;
            this.cSharpButton2.Size = new System.Drawing.Size(109, 36);
            this.cSharpButton2.TabIndex = 1;
            this.cSharpButton2.Text = "cSharpButton2";
            this.cSharpButton2.UseVisualStyleBackColor = true;
            // 
            // cSharpSwtichBox1
            // 
            this.cSharpSwtichBox1.BackColor = System.Drawing.Color.Transparent;
            this.cSharpSwtichBox1.DefaultBitmap = global::CSharpSkin.Demo.Properties.Resources.icon_off_nor;
            this.cSharpSwtichBox1.Location = new System.Drawing.Point(26, 195);
            this.cSharpSwtichBox1.Name = "cSharpSwtichBox1";
            this.cSharpSwtichBox1.Size = new System.Drawing.Size(104, 32);
            this.cSharpSwtichBox1.SwitchImage = global::CSharpSkin.Demo.Properties.Resources.icon_off_hov;
            this.cSharpSwtichBox1.TabIndex = 2;
            this.cSharpSwtichBox1.TabStop = false;
            this.cSharpSwtichBox1.Value = 0;
            // 
            // cSharpImageButton1
            // 
            this.cSharpImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.cSharpImageButton1.CsharpBackgroundImage = global::CSharpSkin.Demo.Properties.Resources.buttn_style_1;
            this.cSharpImageButton1.IsSelected = false;
            this.cSharpImageButton1.Location = new System.Drawing.Point(26, 120);
            this.cSharpImageButton1.Name = "cSharpImageButton1";
            this.cSharpImageButton1.Size = new System.Drawing.Size(128, 44);
            this.cSharpImageButton1.TabIndex = 3;
            this.cSharpImageButton1.Text = "cSharpImageButton1";
            this.cSharpImageButton1.UseVisualStyleBackColor = false;
            this.cSharpImageButton1.XBackImageSplitCount = 4;
            this.cSharpImageButton1.YBackImageSplitCount = 1;
            // 
            // cSharpPictrueButton1
            // 
            this.cSharpPictrueButton1.BackColor = System.Drawing.Color.Transparent;
            this.cSharpPictrueButton1.DefaultBitmap = global::CSharpSkin.Demo.Properties.Resources.icon_upload_nor;
            this.cSharpPictrueButton1.IsSelcted = false;
            this.cSharpPictrueButton1.Location = new System.Drawing.Point(190, 120);
            this.cSharpPictrueButton1.MouseOverImage = global::CSharpSkin.Demo.Properties.Resources.icon_upload_hov;
            this.cSharpPictrueButton1.Name = "cSharpPictrueButton1";
            this.cSharpPictrueButton1.SelectTitleForeColor = System.Drawing.Color.Black;
            this.cSharpPictrueButton1.Size = new System.Drawing.Size(107, 44);
            this.cSharpPictrueButton1.TabIndex = 4;
            this.cSharpPictrueButton1.TabStop = false;
            this.cSharpPictrueButton1.Title = "";
            this.cSharpPictrueButton1.TitleFont = new System.Drawing.Font("Courier New", 12F);
            this.cSharpPictrueButton1.TitleForeColor = System.Drawing.Color.White;
            // 
            // ButtonDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpPictrueButton1);
            this.Controls.Add(this.cSharpImageButton1);
            this.Controls.Add(this.cSharpSwtichBox1);
            this.Controls.Add(this.cSharpButton2);
            this.Controls.Add(this.cSharpButton1);
            this.Name = "ButtonDemo";
            this.Text = "ButtonDemo";
            ((System.ComponentModel.ISupportInitialize)(this.cSharpSwtichBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cSharpPictrueButton1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Button.CSharpButton cSharpButton1;
        private Button.CSharpButton cSharpButton2;
        private Button.CSharpSwtichBox cSharpSwtichBox1;
        private Button.CSharpImageButton cSharpImageButton1;
        private Button.CSharpPictrueButton cSharpPictrueButton1;
    }
}