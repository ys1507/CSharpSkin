﻿namespace CSharpSkin.Demo
{
    partial class SplitContainerDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpSplitContainer1 = new CSharpSkin.SplitContainer.CSharpSplitContainer();
            this.cSharpSplitContainer2 = new CSharpSkin.SplitContainer.CSharpSplitContainer();
            this.cSharpSplitContainer1.Panel2.SuspendLayout();
            this.cSharpSplitContainer1.SuspendLayout();
            this.cSharpSplitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cSharpSplitContainer1
            // 
            this.cSharpSplitContainer1.ArrowColor = System.Drawing.Color.White;
            this.cSharpSplitContainer1.BorderColor = System.Drawing.Color.Red;
            this.cSharpSplitContainer1.CollapsePanel = CSharpSkin.SplitContainer.CollapsePanel.PanelOne;
            this.cSharpSplitContainer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.cSharpSplitContainer1.Location = new System.Drawing.Point(32, 35);
            this.cSharpSplitContainer1.Name = "cSharpSplitContainer1";
            // 
            // cSharpSplitContainer1.Panel2
            // 
            this.cSharpSplitContainer1.Panel2.Controls.Add(this.cSharpSplitContainer2);
            this.cSharpSplitContainer1.Size = new System.Drawing.Size(717, 361);
            this.cSharpSplitContainer1.SplitColor = System.Drawing.Color.Red;
            this.cSharpSplitContainer1.SplitterDistance = 238;
            this.cSharpSplitContainer1.SplitterWidth = 30;
            this.cSharpSplitContainer1.TabIndex = 0;
            // 
            // cSharpSplitContainer2
            // 
            this.cSharpSplitContainer2.ArrowColor = System.Drawing.Color.White;
            this.cSharpSplitContainer2.BorderColor = System.Drawing.Color.Red;
            this.cSharpSplitContainer2.CollapsePanel = CSharpSkin.SplitContainer.CollapsePanel.PanelOne;
            this.cSharpSplitContainer2.Cursor = System.Windows.Forms.Cursors.Default;
            this.cSharpSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cSharpSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.cSharpSplitContainer2.Name = "cSharpSplitContainer2";
            this.cSharpSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.cSharpSplitContainer2.Size = new System.Drawing.Size(449, 361);
            this.cSharpSplitContainer2.SplitColor = System.Drawing.Color.Red;
            this.cSharpSplitContainer2.SplitterDistance = 134;
            this.cSharpSplitContainer2.SplitterWidth = 20;
            this.cSharpSplitContainer2.TabIndex = 0;
            // 
            // SplitContainerDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpSplitContainer1);
            this.Name = "SplitContainerDemo";
            this.Text = "SplitContainerDemo";
            this.cSharpSplitContainer1.Panel2.ResumeLayout(false);
            this.cSharpSplitContainer1.ResumeLayout(false);
            this.cSharpSplitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SplitContainer.CSharpSplitContainer cSharpSplitContainer1;
        private SplitContainer.CSharpSplitContainer cSharpSplitContainer2;
    }
}