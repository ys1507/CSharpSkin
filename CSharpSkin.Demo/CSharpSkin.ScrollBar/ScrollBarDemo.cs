﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSharpSkin.Demo
{
    public partial class ScrollBarDemo : Form
    {
        public ScrollBarDemo()
        {
            InitializeComponent();
        }

        private void ScrollBarDemo_Load(object sender, EventArgs e)
        {
            this.cSharpVScrollBar1.Height = this.cSharpTextBox1.Height;
            //this.cSharpTextBox1.ScrollBars = ScrollBars.Vertical;
        }

        private void CSharpVScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            this.cSharpTextBox1.ScrollToCaret();// e.NewValue
        }
    }
}
