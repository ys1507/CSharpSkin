﻿namespace CSharpSkin.Demo
{
    partial class ScrollBarDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cSharpHScrollBar1 = new CSharpSkin.ScrollBar.CSharpHScrollBar();
            this.cSharpVScrollBar1 = new CSharpSkin.ScrollBar.CSharpVScrollBar();
            this.cSharpTextBox1 = new CSharpSkin.TextBox.CSharpTextBox();
            this.SuspendLayout();
            // 
            // cSharpHScrollBar1
            // 
            this.cSharpHScrollBar1.Location = new System.Drawing.Point(330, 304);
            this.cSharpHScrollBar1.Name = "cSharpHScrollBar1";
            this.cSharpHScrollBar1.ScrollBarColorStyle.BackHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(192)))));
            this.cSharpHScrollBar1.ScrollBarColorStyle.BackNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(249)))), ((int)(((byte)(253)))));
            this.cSharpHScrollBar1.ScrollBarColorStyle.BackPressedColor = System.Drawing.Color.Fuchsia;
            this.cSharpHScrollBar1.ScrollBarColorStyle.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cSharpHScrollBar1.ScrollBarColorStyle.BorderColor = System.Drawing.Color.Fuchsia;
            this.cSharpHScrollBar1.ScrollBarColorStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
            this.cSharpHScrollBar1.ScrollBarColorStyle.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpHScrollBar1.Size = new System.Drawing.Size(349, 17);
            this.cSharpHScrollBar1.TabIndex = 1;
            // 
            // cSharpVScrollBar1
            // 
            this.cSharpVScrollBar1.Location = new System.Drawing.Point(325, 70);
            this.cSharpVScrollBar1.Name = "cSharpVScrollBar1";
            this.cSharpVScrollBar1.ScrollBarColorStyle.BackHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(216)))), ((int)(((byte)(243)))));
            this.cSharpVScrollBar1.ScrollBarColorStyle.BackNormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(249)))), ((int)(((byte)(253)))));
            this.cSharpVScrollBar1.ScrollBarColorStyle.BackPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(202)))), ((int)(((byte)(239)))));
            this.cSharpVScrollBar1.ScrollBarColorStyle.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(230)))), ((int)(((byte)(247)))));
            this.cSharpVScrollBar1.ScrollBarColorStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(210)))), ((int)(((byte)(249)))));
            this.cSharpVScrollBar1.ScrollBarColorStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(135)))), ((int)(((byte)(192)))));
            this.cSharpVScrollBar1.ScrollBarColorStyle.InnerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.cSharpVScrollBar1.Size = new System.Drawing.Size(17, 199);
            this.cSharpVScrollBar1.TabIndex = 0;
            this.cSharpVScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.CSharpVScrollBar1_Scroll);
            // 
            // cSharpTextBox1
            // 
            this.cSharpTextBox1.BorderColor = System.Drawing.Color.Aqua;
            this.cSharpTextBox1.BorderWidth = 1;
            this.cSharpTextBox1.HotColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cSharpTextBox1.Location = new System.Drawing.Point(170, 70);
            this.cSharpTextBox1.Multiline = true;
            this.cSharpTextBox1.Name = "cSharpTextBox1";
            this.cSharpTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.cSharpTextBox1.Size = new System.Drawing.Size(152, 169);
            this.cSharpTextBox1.TabIndex = 2;
            // 
            // ScrollBarDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpTextBox1);
            this.Controls.Add(this.cSharpHScrollBar1);
            this.Controls.Add(this.cSharpVScrollBar1);
            this.Name = "ScrollBarDemo";
            this.Text = "ScrollBarDemo";
            this.Load += new System.EventHandler(this.ScrollBarDemo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ScrollBar.CSharpVScrollBar cSharpVScrollBar1;
        private ScrollBar.CSharpHScrollBar cSharpHScrollBar1;
        private CSharpSkin.TextBox.CSharpTextBox cSharpTextBox1;
    }
}