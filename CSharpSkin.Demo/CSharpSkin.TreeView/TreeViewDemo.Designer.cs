﻿namespace CSharpSkin.Demo
{
    partial class TreeViewDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TreeViewDemo));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("节点0", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("节点0");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("节点2");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("节点0", new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("节点0");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("节点2");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode15,
            treeNode16,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("节点0", new System.Windows.Forms.TreeNode[] {
            treeNode21,
            treeNode22});
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("节点0");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("节点2");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode25,
            treeNode26,
            treeNode27});
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("节点4");
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cSharpTreeView3 = new CSharpSkin.TreeView.CSharpTreeView();
            this.cSharpTreeView2 = new CSharpSkin.TreeView.CSharpTreeView();
            this.cSharpTreeView1 = new CSharpSkin.TreeView.CSharpTreeView();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "tr-node-icon.png");
            this.imageList1.Images.SetKeyName(1, "icon_company_nor.png");
            // 
            // cSharpTreeView3
            // 
            this.cSharpTreeView3.BackColor = System.Drawing.Color.Transparent;
            this.cSharpTreeView3.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.cSharpTreeView3.ExpandIcon = global::CSharpSkin.Demo.Properties.Resources.shrinKIcon;
            this.cSharpTreeView3.FullRowSelect = true;
            this.cSharpTreeView3.ImageIndex = 0;
            this.cSharpTreeView3.ImageList = this.imageList1;
            this.cSharpTreeView3.Indent = 5;
            this.cSharpTreeView3.ItemHeight = 30;
            this.cSharpTreeView3.Location = new System.Drawing.Point(524, 48);
            this.cSharpTreeView3.Name = "cSharpTreeView3";
            treeNode1.Name = "节点3";
            treeNode1.Text = "节点3";
            treeNode2.Name = "节点4";
            treeNode2.Text = "节点4";
            treeNode3.Name = "节点0";
            treeNode3.Text = "节点0";
            treeNode4.Name = "节点1";
            treeNode4.Text = "节点1";
            treeNode5.Name = "节点0";
            treeNode5.Text = "节点0";
            treeNode6.Name = "节点1";
            treeNode6.Text = "节点1";
            treeNode7.Name = "节点2";
            treeNode7.Text = "节点2";
            treeNode8.Name = "节点2";
            treeNode8.Text = "节点2";
            treeNode9.Name = "节点3";
            treeNode9.Text = "节点3";
            treeNode10.Name = "节点4";
            treeNode10.Text = "节点4";
            this.cSharpTreeView3.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4,
            treeNode8,
            treeNode9,
            treeNode10});
            this.cSharpTreeView3.SelectedImageIndex = 0;
            this.cSharpTreeView3.ShrinkIcon = global::CSharpSkin.Demo.Properties.Resources.expandIcon;
            this.cSharpTreeView3.Size = new System.Drawing.Size(239, 353);
            this.cSharpTreeView3.TabIndex = 2;
            // 
            // cSharpTreeView2
            // 
            this.cSharpTreeView2.BackColor = System.Drawing.Color.Transparent;
            this.cSharpTreeView2.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.cSharpTreeView2.ExpandIcon = global::CSharpSkin.Demo.Properties.Resources.shrinKIcon;
            this.cSharpTreeView2.FullRowSelect = true;
            this.cSharpTreeView2.ImageIndex = 0;
            this.cSharpTreeView2.ImageList = this.imageList1;
            this.cSharpTreeView2.ItemHeight = 30;
            this.cSharpTreeView2.Location = new System.Drawing.Point(279, 48);
            this.cSharpTreeView2.Name = "cSharpTreeView2";
            treeNode11.Name = "节点3";
            treeNode11.Text = "节点3";
            treeNode12.Name = "节点4";
            treeNode12.Text = "节点4";
            treeNode13.Name = "节点0";
            treeNode13.Text = "节点0";
            treeNode14.Name = "节点1";
            treeNode14.Text = "节点1";
            treeNode15.Name = "节点0";
            treeNode15.Text = "节点0";
            treeNode16.Name = "节点1";
            treeNode16.Text = "节点1";
            treeNode17.Name = "节点2";
            treeNode17.Text = "节点2";
            treeNode18.Name = "节点2";
            treeNode18.Text = "节点2";
            treeNode19.Name = "节点3";
            treeNode19.Text = "节点3";
            treeNode20.Name = "节点4";
            treeNode20.Text = "节点4";
            this.cSharpTreeView2.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode18,
            treeNode19,
            treeNode20});
            this.cSharpTreeView2.SelectedImageIndex = 0;
            this.cSharpTreeView2.ShrinkIcon = global::CSharpSkin.Demo.Properties.Resources.expandIcon;
            this.cSharpTreeView2.Size = new System.Drawing.Size(239, 353);
            this.cSharpTreeView2.TabIndex = 1;
            this.cSharpTreeView2.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.cSharpTreeView2_NodeMouseClick);
            this.cSharpTreeView2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cSharpTreeView2_MouseClick);
            // 
            // cSharpTreeView1
            // 
            this.cSharpTreeView1.BackColor = System.Drawing.Color.Transparent;
            this.cSharpTreeView1.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawAll;
            this.cSharpTreeView1.ExpandIcon = global::CSharpSkin.Demo.Properties.Resources.shrinKIcon;
            this.cSharpTreeView1.FullRowSelect = true;
            this.cSharpTreeView1.ItemHeight = 30;
            this.cSharpTreeView1.Location = new System.Drawing.Point(34, 48);
            this.cSharpTreeView1.Name = "cSharpTreeView1";
            treeNode21.Name = "节点3";
            treeNode21.Text = "节点3";
            treeNode22.Name = "节点4";
            treeNode22.Text = "节点4";
            treeNode23.Name = "节点0";
            treeNode23.Text = "节点0";
            treeNode24.Name = "节点1";
            treeNode24.Text = "节点1";
            treeNode25.Name = "节点0";
            treeNode25.Text = "节点0";
            treeNode26.Name = "节点1";
            treeNode26.Text = "节点1";
            treeNode27.Name = "节点2";
            treeNode27.Text = "节点2";
            treeNode28.Name = "节点2";
            treeNode28.Text = "节点2";
            treeNode29.Name = "节点3";
            treeNode29.Text = "节点3";
            treeNode30.Name = "节点4";
            treeNode30.Text = "节点4";
            this.cSharpTreeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode23,
            treeNode24,
            treeNode28,
            treeNode29,
            treeNode30});
            this.cSharpTreeView1.ShrinkIcon = global::CSharpSkin.Demo.Properties.Resources.expandIcon;
            this.cSharpTreeView1.Size = new System.Drawing.Size(239, 353);
            this.cSharpTreeView1.TabIndex = 0;
            // 
            // TreeViewDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpTreeView3);
            this.Controls.Add(this.cSharpTreeView2);
            this.Controls.Add(this.cSharpTreeView1);
            this.Name = "TreeViewDemo";
            this.Text = "TreeViewDemo";
            this.Load += new System.EventHandler(this.TreeViewDemo_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private TreeView.CSharpTreeView cSharpTreeView1;
        private System.Windows.Forms.ImageList imageList1;
        private TreeView.CSharpTreeView cSharpTreeView2;
        private TreeView.CSharpTreeView cSharpTreeView3;
    }
}