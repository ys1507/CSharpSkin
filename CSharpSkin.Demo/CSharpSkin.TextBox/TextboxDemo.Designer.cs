﻿namespace CSharpSkin.Demo
{
    partial class TextboxDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextboxDemo));
            this.cSharpTextBox1 = new CSharpSkin.TextBox.CSharpTextBox();
            this.cSharpTextBox2 = new CSharpSkin.TextBox.CSharpTextBox();
            this.cSharpTextBox3 = new CSharpSkin.TextBox.CSharpTextBox();
            this.SuspendLayout();
            // 
            // cSharpTextBox1
            // 
            this.cSharpTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.cSharpTextBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cSharpTextBox1.BackgroundImage")));
            this.cSharpTextBox1.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.cSharpTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cSharpTextBox1.BorderWidth = 3;
            this.cSharpTextBox1.HotColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTextBox1.IsHotTrack = false;
            this.cSharpTextBox1.Location = new System.Drawing.Point(37, 56);
            this.cSharpTextBox1.Name = "cSharpTextBox1";
            this.cSharpTextBox1.Size = new System.Drawing.Size(187, 21);
            this.cSharpTextBox1.TabIndex = 0;
            // 
            // cSharpTextBox2
            // 
            this.cSharpTextBox2.BackColor = System.Drawing.Color.Transparent;
            this.cSharpTextBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cSharpTextBox2.BackgroundImage")));
            this.cSharpTextBox2.BorderColor = System.Drawing.Color.Red;
            this.cSharpTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cSharpTextBox2.BorderWidth = 10;
            this.cSharpTextBox2.HotColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTextBox2.IsHotTrack = false;
            this.cSharpTextBox2.Location = new System.Drawing.Point(37, 94);
            this.cSharpTextBox2.Multiline = true;
            this.cSharpTextBox2.Name = "cSharpTextBox2";
            this.cSharpTextBox2.Size = new System.Drawing.Size(444, 168);
            this.cSharpTextBox2.TabIndex = 1;
            // 
            // cSharpTextBox3
            // 
            this.cSharpTextBox3.BackColor = System.Drawing.Color.Transparent;
            this.cSharpTextBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cSharpTextBox3.BackgroundImage")));
            this.cSharpTextBox3.BorderColor = System.Drawing.Color.Lime;
            this.cSharpTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cSharpTextBox3.BorderWidth = 1;
            this.cSharpTextBox3.HotColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.cSharpTextBox3.IsHotTrack = false;
            this.cSharpTextBox3.Location = new System.Drawing.Point(37, 296);
            this.cSharpTextBox3.Name = "cSharpTextBox3";
            this.cSharpTextBox3.Size = new System.Drawing.Size(187, 21);
            this.cSharpTextBox3.TabIndex = 2;
            // 
            // TextboxDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cSharpTextBox3);
            this.Controls.Add(this.cSharpTextBox2);
            this.Controls.Add(this.cSharpTextBox1);
            this.DoubleBuffered = true;
            this.Name = "TextboxDemo";
            this.Text = "TextboxDemo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CSharpSkin.TextBox.CSharpTextBox cSharpTextBox1;
        private CSharpSkin.TextBox.CSharpTextBox cSharpTextBox2;
        private CSharpSkin.TextBox.CSharpTextBox cSharpTextBox3;
    }
}